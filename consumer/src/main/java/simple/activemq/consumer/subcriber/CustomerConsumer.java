package simple.activemq.consumer.subcriber;

import simple.activemq.consumer.handler.CustomerMessageHandler;
import simple.activemq.producer.constant.JMSConstant;
import simple.activemq.producer.setting.JMSProducerConfiguration;

import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;

public class CustomerConsumer {

    private static CustomerConsumer INSTANCE;
    private final MessageConsumer consumer;

    public static CustomerConsumer getInstance() throws Exception {
        if (INSTANCE == null) INSTANCE = new CustomerConsumer();
        return INSTANCE;
    }

    private CustomerConsumer() throws Exception {
        JMSProducerConfiguration jmsProducerConfiguration = JMSProducerConfiguration.getInstance();
        consumer = createSubscriberMessageType(jmsProducerConfiguration);
    }

    private MessageConsumer createSubscriberMessageType(JMSProducerConfiguration instance) throws Exception {
        Destination destination = instance.createMessageType(JMSConstant.GET_CUSTOMER_QUEUE);
        return instance.session.createConsumer(destination);
    }

    public void subscriberMessage() throws JMSException {
        while (true) {
            CustomerMessageHandler.getInstance(consumer).receiveObjectMessage();
        }
    }
}
