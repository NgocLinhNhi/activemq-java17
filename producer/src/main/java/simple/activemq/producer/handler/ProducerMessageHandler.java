package simple.activemq.producer.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import simple.activemq.producer.entity.CustomerMessage;
import simple.activemq.producer.producer.CustomerMessagePublisher;

import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.Session;
import javax.jms.TextMessage;
import java.util.Locale;

//Java 16 record class
public final record ProducerMessageHandler(Session session,
                                           MessageProducer producer) implements IMessage {

    private static final Logger logger = LoggerFactory.getLogger(CustomerMessagePublisher.class);
    private static ProducerMessageHandler INSTANCE;

    public static ProducerMessageHandler getInstance(Session session, MessageProducer producer) {
        if (INSTANCE == null) INSTANCE = new ProducerMessageHandler(session, producer);
        return INSTANCE;
    }

    @Override
    public void sendObjectMessage() throws Exception {
        //Send Object Message
        ObjectMessage message = session.createObjectMessage(createCustomerMessage());
        logger.info("Message has been Sent !!!!!");
        producer.send(message);
    }

    @Override
    public void sendTextMessage() throws Exception {
        //send textMessage
        String msg = "Hello world ";
        TextMessage message = session.createTextMessage(msg);
        producer.send(message);
        logger.info("Message has been Sent !!!!!");
    }

    private CustomerMessage createCustomerMessage() {
        return new CustomerMessage("VIET", 30, "NHA TRANG");
    }

}
