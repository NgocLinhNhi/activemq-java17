package simple.activemq.producer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ActiveMqJava17Application {

    public static void main(String[] args) {
        SpringApplication.run(ActiveMqJava17Application.class, args);
    }

}
