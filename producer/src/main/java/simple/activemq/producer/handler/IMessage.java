package simple.activemq.producer.handler;

//Java 17 sealed class and permits class
sealed public interface IMessage permits ProducerMessageHandler {
    void sendObjectMessage() throws Exception;

    void sendTextMessage() throws Exception;
}
