package simple.activemq.producer.config;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class ExceptionLogTool {

    //Step 1 : Read Nội dung file in ra dòng có exception => ứng dụng viết tool lọc ra exception từ file Log
    //Tool read log easy =))
    public static void readFile() {
        var temDir = "E:/Project/GitLabProject/active-mq-java-17/activemq-java17/producer/src/main/resources/";
        var fileName = "application.properties";
        try (Stream<String> stream = Files.lines(Paths.get(temDir + fileName), StandardCharsets.UTF_8)) {
            stream.forEach(line -> {
                if (line.toLowerCase().contains("exception")) {
                    System.out.println(line);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
        // Step 2 : read ra rồi ghi hết exception vào 1 file khác đẩy lên folder trên server => cho tester read hàng ngày
    }

    //Read File by Files Java 11
    private static void readFileByJava11() throws IOException {
        var temDir = "E:/Project/GitLabProject/active-mq-java-17/activemq-java17/producer/src/main/resources/";
        var fileName = "application.properties";
        Path filePath = Path.of(temDir + fileName);
        String fileContent = Files.readString(filePath);
        System.out.println(fileContent);
    }

    public static void main(String[] args) throws IOException {
        readFileByJava11();
        readFile();
    }
}
