package simple.activemq.producer.producer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import simple.activemq.producer.handler.ProducerMessageHandler;
import simple.activemq.producer.setting.JMSProducerConfiguration;

import javax.jms.Destination;
import javax.jms.MessageProducer;

import static java.lang.Thread.sleep;
import static simple.activemq.producer.constant.JMSConstant.GET_CUSTOMER_QUEUE;

public class CustomerMessagePublisher {
    private static final Logger logger = LoggerFactory.getLogger(CustomerMessagePublisher.class);

    private static CustomerMessagePublisher INSTANCE;
    private final MessageProducer producer;
    private final JMSProducerConfiguration instance;

    public static CustomerMessagePublisher getInstance() throws Exception {
        if (INSTANCE == null) INSTANCE = new CustomerMessagePublisher();
        return INSTANCE;
    }

    private CustomerMessagePublisher() throws Exception {
        instance = JMSProducerConfiguration.getInstance();
        producer = createPublisherMessageType();
    }

    private MessageProducer createPublisherMessageType() throws Exception {
        Destination destination = instance.createMessageType(GET_CUSTOMER_QUEUE);
        logger.info("Destination has been created with name " + GET_CUSTOMER_QUEUE);
        return instance.session.createProducer(destination);
    }

    public void createMessage() throws Exception {
        while (true) {
            ProducerMessageHandler.getInstance(instance.session, producer).sendObjectMessage();
            //Cứ 10s send message 1 lần
            sleep(10000);
        }
    }


}
