package simple.activemq.consumer.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import simple.activemq.consumer.interfaces.IReceiveMessage;
import simple.activemq.producer.entity.CustomerMessage;

import javax.jms.*;

//Java 16 record class
public record CustomerMessageHandler(MessageConsumer consumer) implements IReceiveMessage {
    private static final Logger logger = LoggerFactory.getLogger(CustomerMessageHandler.class);

    private static CustomerMessageHandler INSTANCE;
    public static CustomerMessageHandler getInstance(MessageConsumer consumer) {
        if (INSTANCE == null) {
            logger.info("Create New connection");
            INSTANCE = new CustomerMessageHandler(consumer);
        }
        return INSTANCE;
    }

    @Override
    public void receiveObjectMessage() throws JMSException {
        //Object message
        Message message = consumer.receive();
        //Java  16 - Pattern matching for instanceof :
        if (message instanceof ObjectMessage objectMessage) {
            CustomerMessage customerMessage = (CustomerMessage) objectMessage.getObject();

            logger.info("Message Subscriber CustomerName : {} ==== CustomerAddress : {} ",
                    //Java 16 record class thay cho get/set
                    customerMessage.customerName(),
                    customerMessage.address());
        }
    }

    @Override
    public void receiveTextMessage() throws JMSException {
        //Text message
        Message message = consumer.receive();
        //Java  16 - Pattern matching for instanceof :
        if (message instanceof TextMessage textMessage) {
            System.out.println(" Received message " + textMessage.getText() + " ");
        }
    }
}
