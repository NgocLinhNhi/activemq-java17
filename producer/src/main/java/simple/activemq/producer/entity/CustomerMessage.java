package simple.activemq.producer.entity;

import java.io.Serializable;

//Java 16 records class thay cho get/set - lombok
public record CustomerMessage(String customerName, int age, String address) implements Serializable {

}
